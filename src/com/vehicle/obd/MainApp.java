package com.vehicle.obd;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import com.vehicle.obd.reader.R;
import com.vehicle.obd.utilities.AppUtilities;
import com.vehicle.obd.utilities.Logger;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/24/13
 * Time: 7:46 PM
 */
public class MainApp extends Application {

    public static String APP_NAME = "";

    //singleton instance
    private static volatile MainApp instance = null;
    private static String buildDateStr = "";
    private final Handler mUIHandler = new Handler(Looper.getMainLooper());
    private boolean mConnectivityAvailable = false;
    private float RESDENSITY;
    private int DENSITY;

    public MainApp() {
        super();
        instance = this;
    }

    /**
     * Double-checked singleton fetching
     * @return
     */
    public static MainApp getInstance() {
        if (instance == null) {
            synchronized(MainApp.class) {
                if (instance == null) {
                    new MainApp();
                }
            }
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.initLogger(this);
        Logger.i("+ MainApp.onCreate(); processors: " + Runtime.getRuntime().availableProcessors());

        initMainAppVars();
        PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Logger.i("+ MainApp.onTerminate()");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Logger.i("+ MainApp.onLowMemory()");
    }

    @TargetApi(8)
    public File getExternalFilesDirAPI8(String type) {
        return super.getExternalFilesDir(type);
    }

    public void runInUIThread(Runnable r) {
        mUIHandler.post(r);
    }

    public String getScreenSizeAsString() {
        int androidScreenSize = getInstance().getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (androidScreenSize) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small_screen";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal_screen";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large_screen";
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return "xlarge_screen";
        }
        return "unknown";
    }

    public String getDPIAsString() {
        if (DENSITY == DisplayMetrics.DENSITY_LOW) {
            return "ldpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_MEDIUM) {
            return "mdpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_HIGH) {
            return "hdpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_XHIGH) {
            return "xhdpi";
        } else if (DENSITY == DisplayMetrics.DENSITY_XXHIGH) {
            return "XX-hdpi";
        } else {
            return "unknown dpi: " + DENSITY;
        }
    }

    private void initMainAppVars() {
        try {
            Resources res = getResources();
            APP_NAME = res.getString(R.string.app_name);
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            DENSITY = displayMetrics.densityDpi;
            RESDENSITY = displayMetrics.density;

            buildDateStr = AppUtilities.readBuildDate();

            AppUtilities.getSSH(this);

        } catch (Throwable th) {
            Logger.e("Fatal error while init main_layout app variables!", th);
            APP_NAME = "+AU_SUR+";
        }
    }
}