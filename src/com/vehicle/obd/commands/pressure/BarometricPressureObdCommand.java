package com.vehicle.obd.commands.pressure;

import com.vehicle.obd.enums.AvailableCommandNames;

/**
 * Barometric pressure.
 */
public class BarometricPressureObdCommand extends PressureObdCommand {

	/**
	 */
	public BarometricPressureObdCommand() {
		super("01 33");
	}

	/**
	 * @param other
	 */
	public BarometricPressureObdCommand(PressureObdCommand other) {
		super(other);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see eu.vehicle.obd.commands.ObdCommand#getName()
	 */
	@Override
	public String getName() {
		return AvailableCommandNames.BAROMETRIC_PRESSURE.getValue();
	}

}