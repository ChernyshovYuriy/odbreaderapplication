package com.vehicle.obd.reader;

import com.vehicle.obd.reader.io.ObdCommandJob;

public interface IPostListener {
	void stateUpdate(ObdCommandJob job);
}