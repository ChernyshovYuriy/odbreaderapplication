package com.vehicle.obd.reader;

import com.vehicle.obd.reader.io.ObdCommandJob;

public interface IPostMonitor {
	void setListener(IPostListener callback);
	boolean isRunning();
	void executeQueue();
	void addJobToQueue(ObdCommandJob job);
}