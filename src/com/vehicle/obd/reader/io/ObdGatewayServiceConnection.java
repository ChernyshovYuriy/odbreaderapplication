package com.vehicle.obd.reader.io;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.vehicle.obd.reader.IPostListener;
import com.vehicle.obd.reader.IPostMonitor;
import com.vehicle.obd.utilities.Logger;

/**
 * Service connection for ObdGatewayService.
 */
public class ObdGatewayServiceConnection implements ServiceConnection {

    private IPostMonitor _service = null;
    private IPostListener _listener = null;

    public void onServiceConnected(ComponentName name, IBinder binder) {
        _service = (IPostMonitor) binder;
        _service.setListener(_listener);
        Logger.d("Service is connected.");
    }

    public void onServiceDisconnected(ComponentName name) {
        _service = null;
        Logger.d("Service is disconnected.");
    }

    /**
     * @return true if service is running, false otherwise.
     */
    public boolean isRunning() {
        if (_service == null) {
            return false;
        }

        return _service.isRunning();
    }

    /**
     * Queue JobObdCommand.
     *
     * @param job
     */
    public void addJobToQueue(ObdCommandJob job) {
        if (null != _service)
            _service.addJobToQueue(job);
    }

    /**
     * Sets a callback in the service.
     *
     * @param listener
     */
    public void setServiceListener(IPostListener listener) {
        _listener = listener;
    }
}