package com.vehicle.obd.reader.views;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.vehicle.obd.utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 11.01.13
 * Time: 17:53
 */
public class AppBaseActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.d("+ " + this.getClass().getSimpleName() + " onCreate, bundle " + bundle);
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.d("+ " + this.getClass().getSimpleName() + " onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d("+ " + this.getClass().getSimpleName() + " onResume");
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.d("+ " + this.getClass().getSimpleName() + " onStop");
    }
}