package com.vehicle.obd.reader.views.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.vehicle.obd.utilities.Logger;

/**
 * According to Android Fragments API, all fragments, including all Dialogs,
 * should have default, public empty constructor.
 * You should use method Fragment.setArguments(Bundle args) if you want to pass some parameters to Fragment,
 * and read this parameters using Fragment.getArguments()
 *
 * User: Hit
 * Date: 09.11.12
 * Time: 16:43
 */
public abstract class BaseDialog extends DialogFragment {

    /**
     * The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it.
     */
    public interface BaseDialogListener {
        public void onDialogPositiveClick(BaseDialog dialog);
        public void onDialogNegativeClick(BaseDialog dialog);
    }

    protected BaseDialogListener mListener;

    public void showNow(FragmentActivity activity) {
        if (activity == null || activity.isFinishing()) {
            Logger.i("Dialog will no be shown, because owner activity is finishing, tag: " + getDialogTag());
            return;
        }
        show(activity.getSupportFragmentManager(), getDialogTag());
    }

    @Override
    public void show(FragmentManager manager, String dialogTag) {
        Logger.d("+ will show dialog: " + dialogTag);
        // to fix IllegalStateException suggested solution from SO implemented:
        // http://stackoverflow.com/a/15229490/810368
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(this, dialogTag);
        transaction.commitAllowingStateLoss();
        //super.show(manager, dialogTag);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the BaseDialogListener so we can send events to the host
            mListener = (BaseDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement BaseDialog.BaseDialogListener" +
                    "interface to be able to show BaseDialog");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.d("+ Dialog started: " + getDialogTag());
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.d("+ Dialog stopped: " + getDialogTag());
    }

    protected void setupDefaultPositiveClickListener(AlertDialog.Builder builder) {
        setupDefaultPositiveClickListener(builder, getString(android.R.string.ok));
    }

    protected void setupDefaultPositiveClickListener(AlertDialog.Builder builder, String positiveBtnText) {
        builder.setPositiveButton(positiveBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Logger.d("+ Dialog accepted: " + getDialogTag());
                // Send the positive button event back to the host activity
                mListener.onDialogPositiveClick(BaseDialog.this);
            }
        });
    }

    protected void setupDefaultNegativeClickListener(AlertDialog.Builder builder) {
        setupDefaultNegativeClickListener(builder, getString(android.R.string.cancel));
    }

    protected void setupDefaultNegativeClickListener(AlertDialog.Builder builder, String negativeBtnText) {
        builder.setNegativeButton(negativeBtnText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Logger.d("+ Dialog dismissed: " + getDialogTag());
                // Send the negative button event back to the host activity
                mListener.onDialogNegativeClick(BaseDialog.this);
            }
        });
    }

    protected void addParcelableConstructorArg(String key, Parcelable value) {
        Bundle args = prepareArgsBundle();
        args.putParcelable(key, value);
        setArguments(args);
    }

    protected void addStringConstructorArg(String key, String value) {
        Bundle args = prepareArgsBundle();
        args.putString(key, value);
        setArguments(args);
    }

    protected void addBooleanConstructorArg(String key, boolean value) {
        Bundle args = prepareArgsBundle();
        args.putBoolean(key, value);
        setArguments(args);
    }

    protected Bundle prepareArgsBundle() {
        Bundle args = getArguments();
        if (args == null) {
            args = new Bundle();
        }
        return args;
    }

    protected String getDialogTag() {
        return this.getClass().getSimpleName();
    }
}