package com.vehicle.obd.reader.views.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 21.05.13
 * Time: 16:36
 */
public class BluetoothDisabledDialog extends BaseDialog {

    public BluetoothDisabledDialog() {
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("You have Bluetooth disabled. Please enable it!");
        setupDefaultPositiveClickListener(builder);
        setupDefaultNegativeClickListener(builder);
        return builder.create();
    }
}