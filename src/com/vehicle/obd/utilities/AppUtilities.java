package com.vehicle.obd.utilities;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.*;
import android.util.Base64;
import android.util.Log;
import com.vehicle.obd.MainApp;
import com.vehicle.obd.reader.R;

import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AppUtilities {

    private static final Random imeiRandom = new Random();

    public static String dumpAllIntentExtras(Intent pushIntent) {
        if (pushIntent == null) {
            return null;
        }
        Bundle extras = pushIntent.getExtras();
        if (extras == null) {
            return "{null}";
        }
        StringBuilder buf = new StringBuilder();
        buf.append("{");
        Iterator<String> keys = extras.keySet().iterator();
        String key;
        String value;
        while (keys.hasNext()) {
            key = keys.next();
            value = extras.getString(key);
            buf.append(key).append(": ").append(value).append(", ");
        }
        int end = buf.length();
        if (end > 2) {
            buf.replace(end - 2, end, "}"); // replacing ", "
        }
        return buf.toString();
    }

    public static void getSSH(Context context) {
        PackageInfo info;
        try {
            info = context.getPackageManager().getPackageInfo("audio.surveillance.AudioSurveillance", PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Logger.d("Hash key: " + something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Logger.e("Hash key name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Logger.e("Hash key No such an algorithm", e.toString());
        } catch (Exception e) {
            Logger.e("Hash key Exception", e.toString());
        }
    }

    public static boolean isRunningUIThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static void removeDir(File root) {
    	if(root.exists()) {
	    	File[] files = root.listFiles();
	    	if(files != null) {
	    		for(File file : files) {
	    			if(file.isDirectory()) {
	    				Logger.i("Dir name: " + file.getName());
	    				removeDir(file);
	    			} else {
	    				Logger.i("Remove file: " + file.getName());
	    				file.delete();
	    			}
	    		}
	    	}
	    	Logger.i("Remove dir: " + root.getName());
	    	root.delete();
    	}
    }

    public static String getApplicationVersion() {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo != null) {
            return packageInfo.versionName;
        } else {
            Logger.d("Can't get application version");
            return "?";
        }
    }

    public static int getApplicationCode() {
        PackageInfo packageInfo = getPackageInfo();
        if (packageInfo != null) {
            return packageInfo.versionCode;
        } else {
            Logger.d("Can't get application code");
            return 0;
        }
    }
    
    public static ApplicationInfo getApplicationInfo() {
    	PackageInfo packageInfo = getPackageInfo();
    	if (packageInfo != null) {
			return packageInfo.applicationInfo;
		} else {
			Logger.d("Can't get application info");
            return null;
		}
    }
	
    /**
     * Checks if the application is in the background (i.e behind another application's Activity).
     * 
     * @param context
     * @return true if another application is above this one.
     */
    private static boolean isApplicationBroughtToBackground(final Context context) {
    	ComponentName topActivity = getTopActivity(context);
        if (topActivity != null && topActivity.getPackageName().equals(context.getPackageName())) {
            return false;
        }
        return true;
    }

	public static ComponentName getTopActivity(Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    List<RunningTaskInfo> tasks = am.getRunningTasks(1);
	    if (tasks != null && !tasks.isEmpty()) {
	    	return tasks.get(0).topActivity;
	    }
	    return null;
	}

	public static boolean isApplicationInBackground() {
		boolean isBg = false;
	    Context context = MainApp.getInstance();
		try {
			isBg = isApplicationBroughtToBackground(context);
		} catch (Exception e) {
			Logger.e("Error in background state check=", e);
		}
		return isBg;
	}

	/**
	 * Read date from /res/raw/date.property and set date to variable.
	 * If we get some error <code>date</code> will have value <code>null</code>
	 */
	public static String readBuildDate() {
        try {
            InputStream ins = MainApp.getInstance().getResources().openRawResource(R.raw.date);
            Properties dateProperty = new Properties();
            dateProperty.load(ins);
            @SuppressWarnings
            ("deprecation") Date _date = new Date(
                Integer.valueOf(dateProperty.getProperty("year")) - 1900,
                Integer.valueOf(dateProperty.getProperty("month")) - 1,
                Integer.valueOf(dateProperty.getProperty("day")),
                Integer.valueOf(dateProperty.getProperty("hour")),
                Integer.valueOf(dateProperty.getProperty("minute")));
            String buildDateStr = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(_date);
            return MainApp.getInstance().getResources().getString(R.string.build_date, buildDateStr);
        } catch (Exception ex) {
            Logger.e("readBuildDate() error", ex);
            return "";
        }
	}

	public static long getAvailableInternalMemorySize() { 
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize(); 
		long availableBlocks = stat.getAvailableBlocks(); 
		return availableBlocks * blockSize; 
	} 
	     
	public static long getTotalInternalMemorySize() { 
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize(); 
		long totalBlocks = stat.getBlockCount(); 
		return totalBlocks * blockSize; 
	}

	public static String roundMemorySize(long memSize) {
		if (memSize < 1024) {
			return memSize + "b";
		}
		if (memSize < 1024*1024) {
			int kbSize = (int) (memSize / 1024);
			return kbSize + "Kb";
		}
		if (memSize < 1024*1024*1024) {
			double mbSize = 100 * memSize / (1024*1024);
			mbSize = Math.round(mbSize) / 100.0;
			return mbSize + "Mb";
		}
		double mbSize = 100 * memSize / (1024*1024*1024);
		mbSize = Math.round(mbSize) / 100.0;
		return mbSize + "Gb";
	} 

	/**
     * @return PackageInfo for the current application or null if the PackageManager could not be contacted.
     */
    private static PackageInfo getPackageInfo() {
        final PackageManager pm = MainApp.getInstance().getPackageManager();
        if (pm == null) {
        	Logger.d("Package manager is NULL");
            return null;
        }
        String packageName = "";
        try {
        	packageName = MainApp.getInstance().getPackageName();
            return pm.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
        	Logger.e("Failed to find PackageInfo : " + packageName);
        	return null;
        } catch (RuntimeException e) {
            // To catch RuntimeException("Package manager has died") that can occur on some version of Android,
            // when the remote PackageManager is unavailable. I suspect this sometimes occurs when the App is being reinstalled.
        	Logger.e("Package manager has died : " + packageName);
            return null;
        } catch (Throwable e) {
        	Logger.e("Package manager has Throwable : " + e);
            return null;
        }
    }

    public static boolean externalStorageAvailable() {
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        return mExternalStorageAvailable && mExternalStorageWriteable;
    }

    private static void printAllStacktraces(StringBuilder buf) throws Exception {
        Set<Map.Entry<Thread, StackTraceElement[]>> entries = Thread.getAllStackTraces().entrySet();
        int threadsCnt = entries.size();
        Iterator<Map.Entry<Thread, StackTraceElement[]>> iterator = entries.iterator();
        buf.append("\nThreads count: ").append(threadsCnt);
        while (iterator.hasNext()) {
            Map.Entry<Thread, StackTraceElement[]> entry = iterator.next();
            Thread thread = entry.getKey();
            buf.append('\n').append(thread.getName()).append(" (grp:").append(thread.getThreadGroup().getName()).append(")")
                    .append(" prio=").append(thread.getPriority())
                    .append(" tid=").append(thread.getId())
                    .append(" - ").append(thread.getState());
            if (thread.isDaemon()) {
                buf.append(" (daemon)");
            }
            buf.append('\n');
            StackTraceElement[] trace = entry.getValue();
            for (StackTraceElement line : trace) {
                buf.append("    at ").append(line).append('\n');
            }
        }
    }

    public static String getExternalStorageDir() {
        if (Build.VERSION.SDK_INT < 8) {
            return Environment.getExternalStorageDirectory().getAbsolutePath() + "/AudioSurveillance/" + getPackageInfo().packageName;
        } else {
            File externalDir = MainApp.getInstance().getExternalFilesDirAPI8(null);
            return externalDir != null ? externalDir.getAbsolutePath() : null;
        }
    }

    public static String combinePath (String path1, String path2) {
        File file1 = new File(path1);
        File file2 = new File(file1, path2);
        return file2.getPath();
    }

    public static String combinePath (String path1, String path2, String path3) {
        return combinePath(combinePath(path1, path2), path3);
    }

    public static String buildDeviceStr() {
        return Build.BRAND + "-" + Build.DEVICE + "-" + Build.MODEL;
    }

    public static String getCurrentCallStack() {
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] cause = Thread.currentThread().getStackTrace();
        for (StackTraceElement ste : cause) {
            sb.append(ste.toString() + '\n');
        }
        return  sb.toString();
    }

    /*public static String composeIMEI() {
        String imei = TelephonyManagerAssistant.getDeviceId();
        if ("000000000000000".equals(imei) || StringUtils.isEmpty(imei)) {
            String aid = android.provider.Settings.Secure.getString(MainApp.getInstance().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            if (StringUtils.isNotEmpty(aid) && !"android_id".equalsIgnoreCase(aid)) {
                imei = "aid-" + aid;
            } else {
                int rnd = imeiRandom.nextInt(999999);
                if (imei == null) {
                    imei = "";
                }
                imei += rnd;
                imei = "rnd-" + imei;
            }
        }
        return imei;
    }*/

    /**
     * Perform URL encoding
     */
    public static String urlEncoding(String str){
        if (str == null) {
            return "";
        }
        char[]  change = {'%','+', ' ','{','}','|','\\','^','~','[',']','`','#','<','>','/','?',':','@','&',';','\"','=','\n','\r'};
        for (int i = 0; i < change.length; i++) {
            String ch = String.valueOf(change[i]);
            String encCh = Integer.toHexString((int) change[i]);
            if (encCh.length() < 2) encCh = "0" + encCh;
            int ind = 0;
            while (str.indexOf(ch,ind) != -1) {
                ind = str.indexOf(ch,ind)+1;
                str = str.substring(0,ind-1) + "%" + encCh + str.substring(ind);
            }
        }
        return str;
    }
}