package com.vehicle.obd.utilities;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Debug;
import android.util.Log;
import com.vehicle.obd.MainApp;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Logger {

    public static final String LOG_TAG = "ODB_READER_APP";
	public static final String ERROR_LOG_PREFIX = "LOG_ERR: ";
	private static final String LOG_FILENAME = "ODBReaderAppLog.log";
	private static final int MAX_BACKUP_INDEX = 3;
	private static final String MAX_FILE_SIZE = "750KB";

    private static String initLogsDirectory;
	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

	//TODO
	//Implement LogCat appender
	//Need to be called from main_layout views onCreate;
	//Implement Properties from File
	public static void initLogger(Context context) {
        initLogsDirectories(context);
		String fileName = getCurrentLogsDirectory() + "/" + LOG_FILENAME;
		logger.setLevel(Level.DEBUG);
		org.apache.log4j.Layout layout = new PatternLayout("%d [%t] %-5p %m%n");
        try {
            logger.removeAllAppenders();
        } catch (Exception e) {
            Logger.e("Unable to remove logger uppenders.");
        }
		try {
			RollingFileAppender rollingFileAppender = new RollingFileAppender(layout, fileName);
			rollingFileAppender.setMaxFileSize(MAX_FILE_SIZE);
			rollingFileAppender.setMaxBackupIndex(MAX_BACKUP_INDEX);
			logger.addAppender(rollingFileAppender);
		} catch(IOException ioe) {
			Log.e("ODB_READER_APP", "unable to create log file: " + fileName);
		}
        Logger.d("Current log stored to " + fileName);
	}


    private static void initLogsDirectories(Context context) {
        initLogsDirectory = context.getFilesDir() + "/logs";
    }
        
    public static String getCurrentLogsDirectory() {
        if (AppUtilities.externalStorageAvailable()) {
            String extLogsDirectory = AppUtilities.getExternalStorageDir();
            if (StringUtils.isNotEmpty(extLogsDirectory)) {
                return extLogsDirectory + "/logs";
            }
        }
        return initLogsDirectory;
    }

    public static boolean isUsingExternalStorage() {
        return AppUtilities.externalStorageAvailable();
    }
	
	public static File[] getLogsDirectories() {
        if (AppUtilities.externalStorageAvailable()) {
            String extLogsDirectory = AppUtilities.getExternalStorageDir();
            if (StringUtils.isNotEmpty(extLogsDirectory)) {
                return new File[] {new File(initLogsDirectory), new File(extLogsDirectory + "/logs")};
            }
        }
		return new File[] {new File(initLogsDirectory)} ;
	}

    public static File[] getAllLogs() {
        ArrayList<File> logs = new ArrayList<File>();
        File[] logDirs = getLogsDirectories();
        for (File dir: logDirs) {
            if (dir.exists()) {
                logs.addAll(Arrays.asList(getLogs(dir)));
            }
        }
        return logs.toArray(new File[logs.size()]);
    }

    public static File[] getInternalLogs() {
        return getLogs(new File(initLogsDirectory));
    }

    private static File[] getLogs(File directory) {
        if (directory.isFile()) {
            throw new IllegalArgumentException("directory is not folder " + directory.getAbsolutePath());
        }
        return directory.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                if (name != null && name.toLowerCase().endsWith(".log")) {
                    return true;
                }
                for (int i = 1; i <= MAX_BACKUP_INDEX; i++) {
                    if (name != null && name.toLowerCase().endsWith(".log." + i)) {
                        return true;
                    }
                }
                return false;
            }
        });        
    }

    public static void writeCurrentLog(FileOutputStream outputStream) throws IOException {
        //Appends in the tail of every log.
        outputStream.write(getAdditionInfo().getBytes());

        File[] logs = getAllLogs();
        PrintWriter printWriter = new PrintWriter(outputStream);

        for (File log : logs) {
            BufferedReader br = new BufferedReader(new FileReader(log.getPath()));
            String line = br.readLine();
            while (line != null) {
                printWriter.println(line);
                line = br.readLine();
            }
            br.close();
        }
        printWriter.flush();
    }
		
	public static void e(String logMsg) {
        e(logMsg, (Throwable) null);
	}

	public static void w(String logMsg) {
        w(logMsg, (Throwable) null);
	}

	public static void i(String logMsg) {
        i(logMsg, (Throwable) null);
	}

	public static void d(String logMsg) {
        d(logMsg, (Throwable) null);
	}

    public static void e(String logPrefix, String logMsg) {
    	e(logPrefix + logMsg);
    }

    public static void w(String logPrefix, String logMsg) {
    	w(logPrefix + logMsg);
    }

    public static void i(String logPrefix, String logMsg) {
    	i(logPrefix + logMsg);
    }

    public static void d(String logPrefix, String logMsg) {
    	d(logPrefix + logMsg);
    }

    public static void e(String logMsg, Throwable t) {
        logMsg = ERROR_LOG_PREFIX + logMsg;
        if (t != null) {
            logger.error(logMsg, t);
            Log.e(LOG_TAG, logMsg, t);
        } else {
            logger.error(logMsg);
            Log.e(LOG_TAG, logMsg);
        }
    }

    public static void w(String logMsg, Throwable t) {
        if (t != null) {
            //logger.warn(logMsg, t);
            Log.w(LOG_TAG, logMsg, t);
        } else {
            //logger.warn(logMsg);
            Log.w(LOG_TAG, logMsg);
        }
    }

    public static void i(String logMsg, Throwable t) {
        if (t != null) {
            logger.info(logMsg, t);
            Log.i(LOG_TAG, logMsg, t);
        } else {
            logger.info(logMsg);
            Log.i(LOG_TAG, logMsg);
        }
    }

    public static void d(String logMsg, Throwable t) {
        if (t != null) {
            //logger.debug(logMsg, t);
            Log.d(LOG_TAG, logMsg, t);
        } else {
            //logger.debug(logMsg);
            Log.d(LOG_TAG, logMsg);
        }
    }

    /**
     * Return string with addition info about device, application name, license
     * and other.
     *
     * @return String with addition info (App name, IMEI, version, License ...)
     */
    private static String getAdditionInfo() {
        StringBuilder addInfo = new StringBuilder(1000);
        //String storedIMEI = AppSettingsDataBaseManager.getAppParam(MainApp.getInstance(), AppEntry.COLUMN_NAME_IMEI);

        addInfo.append("App name: ").append(MainApp.APP_NAME).append("\n");
        addInfo.append("App version: ").append(AppUtilities.getApplicationVersion()).append(";  ").append(AppUtilities.readBuildDate()).append("\n");
        //addInfo.append("IMEI: ").append(storedIMEI).append("\n");
        ApplicationInfo applicationInfo = AppUtilities.getApplicationInfo();
        if (applicationInfo != null) {
            addInfo.append("App dir: ").append(applicationInfo.dataDir).append("\n");
            addInfo.append("App source dir: ").append(applicationInfo.sourceDir).append("\n");
            addInfo.append("App public source dir: ").append(applicationInfo.publicSourceDir).append("\n");
        }
        addInfo.append("\n------- Device -----------\n");
        addInfo.append("Brand: ").append(Build.BRAND).append("\n");
        addInfo.append("Board: ").append(Build.BOARD).append("\n");
        addInfo.append("Device: ").append(Build.DEVICE).append("\n");
        addInfo.append("Model: ").append(Build.MODEL).append("\n");
        addInfo.append("Id: ").append(Build.ID).append("\n");
        addInfo.append("Product: ").append(Build.PRODUCT).append("\n");
        addInfo.append("Display: ").append(Build.DISPLAY).append("\n");
        addInfo.append("--------- Firmware ------------\n");
        addInfo.append("SDK: ").append(Build.VERSION.SDK_INT).append("\n");
        addInfo.append("Release: ").append(Build.VERSION.RELEASE).append("\n");
        addInfo.append("Tags: ").append(Build.TAGS).append("\n");
        addInfo.append("Incremental: ").append(Build.VERSION.INCREMENTAL).append("\n");
        addInfo.append("--------- Misc -------------\n");
        addInfo.append("Total space: ").append(AppUtilities.roundMemorySize(AppUtilities.getTotalInternalMemorySize())).append("\n");
        addInfo.append("Avail space: ").append(AppUtilities.roundMemorySize(AppUtilities.getAvailableInternalMemorySize())).append("\n");
        addInfo.append("Total memory: ").append(AppUtilities.roundMemorySize(Runtime.getRuntime().totalMemory())).append("\n");
        addInfo.append("Avail memory: ").append(AppUtilities.roundMemorySize(Runtime.getRuntime().freeMemory())).append("\n");
        addInfo.append("Native memory: ").append(AppUtilities.roundMemorySize(Debug.getNativeHeapAllocatedSize())).append("\n");
        addInfo.append("Screen size: ").append(MainApp.getInstance().getScreenSizeAsString()).append("\n");
        addInfo.append("DPI: ").append(MainApp.getInstance().getDPIAsString()).append("\n");
        addInfo.append("-------------------------------\n\n");
        return addInfo.toString();
    }
}