package com.vehicle.obd.utilities;

import java.util.concurrent.ThreadFactory;

/**
 * Author: Valery Bogdanov
 * Date: 11.01.13
 * Time: 9:57
 */
public class NamedThreadFactory implements ThreadFactory {

    private final String mThreadsNamePrefix;
    private int mThreadsCounter;
    private int mPriority = -1;

    public NamedThreadFactory(String threadsNamePrefix) {
        mThreadsNamePrefix = threadsNamePrefix;
        mThreadsCounter = 0;
    }

    public NamedThreadFactory(String threadsNamePrefix, int priority) {
        mThreadsNamePrefix = threadsNamePrefix;
        mThreadsCounter = 0;
        mPriority = priority;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        mThreadsCounter++;
        Thread thread = new Thread(runnable, mThreadsNamePrefix + "-t#" + mThreadsCounter);
        if(mPriority != -1) {
            thread.setPriority(mPriority);
        }
        return thread;
    }
}