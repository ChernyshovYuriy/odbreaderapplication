package com.vehicle.obd.utilities;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.vehicle.obd.MainApp;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 6/12/12
 * Time: 4:25 PM
 */
public class TelephonyManagerAssistant {

    public TelephonyManagerAssistant() {

    }

    public String getPhoneLine1Number() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getLine1Number();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getPhoneLine1Number error", throwable);
            }
        }
        return value;
    }

    public String getDeviceSimCountryIso() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getSimCountryIso();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceSimCountryIso error", throwable);
            }
        }
        return value;
    }

    public String getDeviceSimOperatorName() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getSimOperatorName();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceSimOperatorName error", throwable);
            }
        }
        return value;
    }

    public String getDeviceSimOperator() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getSimOperator();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceSimOperator error", throwable);
            }
        }
        return value;
    }

    public int getDeviceSimState() {
        TelephonyManager telephonyManager = getTelephonyManager();
        int value = 0;
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getSimState();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceSimState error", throwable);
            }
        }
        return value;
    }

    public boolean isDeviceNetworkRoaming() {
        TelephonyManager telephonyManager = getTelephonyManager();
        Boolean value = false;
        if (telephonyManager != null) {
            try {
                value = telephonyManager.isNetworkRoaming();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->isDeviceNetworkRoaming error", throwable);
            }
        }
        return value;
    }

    public int getDevicePhoneType() {
        TelephonyManager telephonyManager = getTelephonyManager();
        int value = 0;
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getPhoneType();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDevicePhoneType error", throwable);
            }
        }
        return value;
    }

    public String getDeviceNetworkOperator() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getNetworkOperator();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceNetworkOperator error", throwable);
            }
        }
        return value;
    }

    public String getDeviceNetworkOperatorName() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getNetworkOperatorName();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceNetworkOperatorName error", throwable);
            }
        }
        return value;
    }

    public static String getDeviceId() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getDeviceId();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceId error", throwable);
            }
        }
        return value;
    }

    public static String getDeviceNetworkCountryIso() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getNetworkCountryIso();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getDeviceNetworkCountryIso error", throwable);
            }
        }
        return value;
    }

    public String getSimCountryCode() {
        TelephonyManager telephonyManager = getTelephonyManager();
        String value = "";
        if (telephonyManager != null) {
            try {
                value = telephonyManager.getSimCountryIso();
            } catch (Throwable throwable) {
                Logger.e("TelephonyManagerAssistant->getSimCountryCode error", throwable);
            }
        }
        return value;
    }

    private static TelephonyManager getTelephonyManager() {
        return (TelephonyManager) MainApp.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
    }
}